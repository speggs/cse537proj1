# search.py

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):    
    from game import Directions
    n = Directions.NORTH
    e = Directions.EAST
    s = Directions.SOUTH
    w = Directions.WEST
	
    path = []
    curNode = problem.getStartState()
    if problem.isGoalState(curNode):
        return path

    #stack of nodes to visit
    toVisit = util.Stack()
    #set of visited nodes, no need to visit the same node twice
    visited = set()
    #prevMoves and prevNodes are used to reconstruct the path to the goal once the goal is reached. They map nodes to the move which took pacman to that node and the nodes to the preceding node respectively
    prevMoves = dict()
    prevNodes = dict()
	
    #to begin the search we push the start node onto the toVisit stack
    toVisit.push(curNode)
    
    while not toVisit.isEmpty():
        #the node we are currently considering
        curNode = toVisit.pop()
        # print "curNode:", curNode
        #no need to visit a node twice
        visited.add(curNode)
        #if we have reached our goal, reconstruct the path we took and return it
        if problem.isGoalState(curNode):
            return reconstructPath(prevMoves, prevNodes, curNode, problem)

        #if we have reached here then curNode is not the goal, therefore expand it's successors and push them onto the toVisit stack IF we have not already visited them
        #get successors returns list of (node, move, cost)
        for succ in problem.getSuccessors(curNode):
            if not succ[0] in visited:
                toVisit.push(succ[0])
                visited.add(succ[0])
                prevMoves[succ[0]] = succ[1]
                prevNodes[succ[0]] = curNode

#this function reconstructs the path taken from starting node to goalNode using two dictionaries which map nodes to their preceding move/node.
def reconstructPath(prevMoves, prevNodes, goalNode, problem):
    ret = []
    prevNode = prevNodes[goalNode]
    prevMove = prevMoves[goalNode]
    #start from goal node
    ret.append(prevMove)
    while not prevNode == problem.getStartState():
        prevMove = prevMoves[prevNode]
        prevNode = prevNodes[prevNode]
        #build list of moves
        ret.append(prevMove)
    #since the list of actions is built backwards (from goal to start) we must reverse the list
    ret.reverse()
    return ret

#the only difference between breadth first search and depth first search is that toVisit is Queue not a stack (FIFO not LIFO), whereas a path is considered until it reaches a dead end or goal in DFS, and then retraced back until an unexplored fork is found, this function explores all forks with equal priority as they are uncovered. It has a higher cost in memory, but guarantees an optimal solution.
def breadthFirstSearch(problem):    
    from game import Directions
    n = Directions.NORTH
    e = Directions.EAST
    s = Directions.SOUTH
    w = Directions.WEST
	
    path = []
    curNode = problem.getStartState()
    if problem.isGoalState(curNode):
        return path

    #Queue not Stack
    toVisit = util.Queue()
    visited = set()
    prevMoves = dict()
    prevNodes = dict()
	
    toVisit.push(curNode)
    
    while not toVisit.isEmpty():
        curNode = toVisit.pop()
        #print "curNode:", curNode
        visited.add(curNode)
        if problem.isGoalState(curNode):
            return reconstructPath(prevMoves, prevNodes, curNode, problem)

        for succ in problem.getSuccessors(curNode):
            if not succ[0] in visited:
                toVisit.push(succ[0])
                visited.add(succ[0])
                prevMoves[succ[0]] = succ[1]
                prevNodes[succ[0]] = curNode

#The only difference between uniformCostSearch and breadthFirstSearch is that we use a PriorityQueue for toVisit where the priority of inserted nodes is the cost of the node as determined by the problem's cost function.
def uniformCostSearch(problem):    
    from game import Directions
    n = Directions.NORTH
    e = Directions.EAST
    s = Directions.SOUTH
    w = Directions.WEST
	
    path = []
    curNode = problem.getStartState()
    if problem.isGoalState(curNode):
        return path

    #PriorityQueue not Queue
    #TODO: the fact that they provide an update function implies that I might need to be updating priorities if a shorter path is found?
    toVisit = util.PriorityQueue()
    visited = set()
    prevMoves = dict()
    prevNodes = dict()
    priorities = dict()
	
    #use the costFn to determine priority of node
    cost = problem.costFn(curNode)
    toVisit.push(curNode, problem.costFn(curNode))
    priorities[curNode] = 0 + problem.costFn(curNode)
    
    while not toVisit.isEmpty():
        curNode = toVisit.pop()
     #   print "curNode:", curNode
        visited.add(curNode)
        if problem.isGoalState(curNode):
            return reconstructPath(prevMoves, prevNodes, curNode, problem)

        for succ in problem.getSuccessors(curNode):
            cost = priorities[curNode] + problem.costFn(succ[0])
            
            if not succ[0] in visited:
                toVisit.push(succ[0], cost)
                visited.add(succ[0])
                prevMoves[succ[0]] = succ[1]
                prevNodes[succ[0]] = curNode
                priorities[succ[0]] = cost
            elif cost < priorities[succ[0]]:
                toVisit.update(succ[0], cost)
                prevMoves[succ[0]] = succ[1]
                prevNodes[succ[0]] = curNode
                priorities[succ[0]] = cost

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

#the only difference between this and uniformCostSearch is that it use the function parameter heuristic to estimate the total cost of the path to the goal through the node being considered by estimating the cost of the rest of the path through the considered node with the function parameter heuristic.
def aStarSearch(problem, heuristic=nullHeuristic):    
    from game import Directions
    n = Directions.NORTH
    e = Directions.EAST
    s = Directions.SOUTH
    w = Directions.WEST
	
    path = []
    curNode = problem.getStartState()
    if problem.isGoalState(curNode):
        return path

    toVisit = util.PriorityQueue()
    visited = set()
    closed = set()
    prevMoves = dict()
    prevNodes = dict()
    fscores = dict()
    gscores = dict()
	
    #use the costFn + heuristic to determine priority of node.
    #costEstimate = problem.costFn(curNode) + heuristic(curNode, problem)
    gcost = heuristic(curNode, problem)
    #estimate cost of path through node
    #f(n) = g(n) + h(n)
    fscores[curNode] = gcost
    #cost from start to this node
    gscores[curNode] = 0
    visited.add(curNode)
    toVisit.push(curNode, gcost)
    
    while not toVisit.isEmpty():
        curNode = toVisit.pop()
     #   print "curNode:", curNode
        closed.add(curNode)
        if problem.isGoalState(curNode):
            return reconstructPath(prevMoves, prevNodes, curNode, problem)

        for succ in problem.getSuccessors(curNode):
            if succ[0] in closed:
                continue
            gcost = gscores[curNode] + 1
            if not succ[0] in visited:
                toVisit.push(succ[0], gcost + heuristic(succ[0], problem))
                visited.add(succ[0])
                prevMoves[succ[0]] = succ[1]
                prevNodes[succ[0]] = curNode
                gscores[succ[0]] = gcost
            elif gcost < gscores[succ[0]]:
                toVisit.update(succ[0], gcost + heuristic(succ[0], problem))
                prevMoves[succ[0]] = succ[1]
                prevNodes[succ[0]] = curNode
                gscores[succ[0]] = gcost


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
